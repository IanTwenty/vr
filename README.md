<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

Recover from vim crashes

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/vr/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# vr

Clean up old swap files from vim crashes.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Install](#install)
* [Usage](#usage)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

When vim or the system crashes I like to deal with any outstanding vim swap
files immediately so I know files are back as they should be. This BASH script
iterates through all vim swap files, starts a vim recover session for each and
then gives you the option to clear up the swap file afterward.

## Install

To install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/vr
```

To install manually git clone this repo. Clone a tag to ensure you're on an
official version. Optionally copy `vr` to a dir on your PATH, e.g.
`/usr/local/bin`.

## Usage

After a crash just run it:

``
vr
``

It will walk you through recovery of each edited file for which a swap file was
found:

* Launch vim in recovery mode with the next swap file
* During this session you should decide if you need to save or revert any
  unsaved changes. You can also use `:DiffOrig` to see how the recovered buffer
  differs from that on disk. Once finished quit the session.
* `vr` will then check if the swap file is still present. If so you can delete
  it if you have no further use for it.

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* All source code is licensed under GPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

galagos is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
