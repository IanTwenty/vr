#!/bin/bash

# SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Recover from vim crashes
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/IanTwenty/vr/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# Find vim swap location
tmpfile=$(mktemp)
vim=$(which vim)
"$vim" -c "0,\$d | put =&directory| 1d | w | q" "$tmpfile" || exit $?
swapdir=$(cat "$tmpfile")

if [[ ! -d "$swapdir" ]]; then
  echo "Vim reported swap dir as $swapdir but this is not a dir"
  exit 1
fi

mapfile -t SWAP_FILES < <(find "$swapdir" -type f)

for ((i = 0; i < ${#SWAP_FILES[@]}; i++)) do
  echo "Found swap: ${SWAP_FILES[$i]}"
  "$vim" -r "${SWAP_FILES[$i]}"
  if [[ -f "${SWAP_FILES[$i]}" ]]; then
    read -rp "  Vim left the swap file behind still. What do you want to do with it - (d)elete swap file, (c)ontinue?" answer
    case $answer in
      [dD]* ) rm "${SWAP_FILES[$i]}" || exit 1
              continue;;
      [cC]* ) continue;;
      * ) echo "Couldn't understand answer: $answer" && exit 1
    esac
  fi
done
